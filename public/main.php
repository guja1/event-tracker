<?php

require_once __DIR__ . '/../app/DotEnv.php';
require_once __DIR__ . '/../app/App.php';

(new DotEnv(getenv('ENV_PATH') ?: realpath(dirname(__DIR__) . '/.env')))->load();

$app = new App(dirname(__DIR__));

while($line = $app->reader()->readline()) {
    // parse log
    $log = json_decode($line, true);

    $url = explode(' ', $log['request'] ?? '', 3)[1] ?? null;

    $queryString = parse_url($url, PHP_URL_QUERY);

    parse_str($queryString, $queryData);

    // validate log
    if(!key_exists('act', $queryData) || !key_exists('cat', $queryData) || !key_exists('site', $queryData)) {
        continue;
    }

    // build data model
    $data = [
        'site_id' => $queryData['site'],
        'action' => $queryData['act'],
        'category' => $queryData['cat'],
        'custom' => key_exists('cus', $queryData) ? json_decode($queryData['cus'], true) : null,
        'referrer' => $queryData['ref'] ?? null,
        'url' => $queryData['url'] ?? null,
        'campaign' => $queryData['cam'] ?? null,
        'client' => [
            'session_id' => $queryData['sid'] ?? null,
            'user_id' => $queryData['uid'] ?? null,
            'ip' => $log['remote_addr'] ?? null,
            'user_agent' => $log['http_user_agent'],

            'country' => $log['country'] ?? '',
            'region' => $log['region'] ?? '',
            'city' => $log['city'] ?? '',
        ],
        'occured_at' => strtotime($log['@timestamp']),
    ];

    $data = array_filter($data);
    $data['client'] = array_filter($data['client']);

    $app->esIndex()->save($data);
}

