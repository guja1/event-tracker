<?php

require_once __DIR__ . '/../app/ContinuouslyFileReader.php';
require_once __DIR__ . '/../app/ESIndex.php';

class App
{
    protected $baseDirectory;
    protected $publicPath;
    protected $storagePath;

    protected $instances = [];

    public function __construct(string $baseDirectory)
    {
        $this->baseDirectory = rtrim(realpath($baseDirectory), DIRECTORY_SEPARATOR);
        $this->publicPath = $this->baseDirectory . DIRECTORY_SEPARATOR . 'public';
        $this->storagePath = $this->baseDirectory . DIRECTORY_SEPARATOR . 'storage';
    }

    public function join(string $p1, string $p2)
    {
        return rtrim($p1, DIRECTORY_SEPARATOR)
            . DIRECTORY_SEPARATOR
            . ltrim($p2, DIRECTORY_SEPARATOR);
    }

    public function base_path($path = '')
    {
        return $this->join($this->baseDirectory, $path);
    }

    public function public_path($path = '')
    {
        return $this->join($this->publicPath, $path);
    }

    public function storage_path($path = '')
    {
        return $this->join($this->storagePath, $path);
    }

    /**
     * @return ESIndex
     */
    public function esIndex()
    {
        if (!key_exists(ESIndex::class, $this->instances)) {
            $this->instances[ESIndex::class] = new ESIndex(getenv('ES_URL'), getenv('ES_INDEX') ?: 'events');
        }

        return $this->instances[ESIndex::class];
    }

    /**
     * @return ContinuouslyFileReader
     */
    public function reader()
    {
        if (!key_exists(ContinuouslyFileReader::class, $this->instances)) {
            $logFilePath = realpath(getenv('NGINX_LOG_PATH'));

            $this->instances[ContinuouslyFileReader::class] =
                new ContinuouslyFileReader(
                    $logFilePath,
                    1,
                    $this->storage_path('seek'));
        }

        return $this->instances[ContinuouslyFileReader::class];
    }
}