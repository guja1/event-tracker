<?php

class SeekManager
{
    protected $filePath;
    protected $resource;
    protected $current;

    public function __construct($filePath = "seek")
    {
        $this->filePath = $filePath;

        $this->current = 0;
        if (!file_exists($this->filePath)) {
            touch($this->filePath);
        }

        $this->resource = fopen($this->filePath, 'r+');
        $this->current = intval(fgets($this->resource));
    }

    public function Seek(int $seek = null): int
    {
        if (is_null($seek)) {
            return $this->current;
        }

        fseek($this->resource, 0);
        ftruncate($this->resource, strlen((string) $this->current));
        fwrite($this->resource, (string) $seek);
        $this->current = $seek;
        unset($content);
        unset($seek);

        return $this->current;
    }
}
