<?php

class ESIndex
{
    protected $index;
    protected $url;

    public function __construct($url, $index)
    {
        $this->url = rtrim($url, '/');
        $this->index = trim($index, '/');
    }

    public function save($data)
    {
        return $this->send($this->path('_doc'), $data);
    }

    protected function path($path = '')
    {
        return rtrim(sprintf('%s/%s/%s', $this->url, $this->index, $path), '/');
    }

    protected function send($url, $data)
    {
        $ch = curl_init($url);

        # Setup request to send json via POST.
        $payload = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            // 'Content-Length: ' . strlen($payload),
        ]);

        # Return response instead of printing.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);

        # Send request.
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}