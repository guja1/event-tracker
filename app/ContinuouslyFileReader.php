<?php

require_once __DIR__ . '/SeekManager.php';

/**
 * @example
 * $reader = new ContinuouslyFileReader(__DIR__ . DIRECTORY_SEPARATOR . "test.txt", 1);
 * while($line = $reader->readline()) {
 *     echo $line;
 * }
 */
class ContinuouslyFileReader
{
    protected $filePath;
    protected $watchInterval;
    protected $seekManager;
    protected $resource;
    protected $filemtime;

    /**
     * @param string $filePath - real path
     * @param int $watchInterval - represents seconds
     * @param string|null $workDir - gets $filePath directory by default, if specified it must be non relative path
     */
    public function __construct(string $filePath, int $watchInterval = 1, string $workDir = null)
    {
        // ensure it's real path
        $filePath = realpath($filePath);

        if (!is_readable($filePath)) {
            throw new Exception('Target file is not readable or does not exists');
        }


        if (is_null($workDir)) {
            $workDir = dirname($filePath);
            $seekFolderPath = $workDir;
        } else {
            // ensure workdir has no DIRECTORY_SEPARATOR at the end
            $workDir = rtrim($workDir, DIRECTORY_SEPARATOR);
            /*
             * filename is not unique, to be sure that it will not make some issues
             * we make folder based on file real path inside working directory
             * 
             * in case $workDir is not present seek folder has no value
             */
            $seekFolderPath = $workDir . DIRECTORY_SEPARATOR . md5(dirname($filePath));
        }


        if (!is_writable($workDir)) {
            throw new Exception("Working directory is not writable [{$workDir}]");
        }

        if (!file_exists($seekFolderPath)) {
            mkdir($seekFolderPath);
        }

        $seekManagerFile = $seekFolderPath . DIRECTORY_SEPARATOR . basename($filePath) . '.seek';

        $this->filePath = $filePath;
        $this->watchInterval = $watchInterval;

        $this->filemtime = filemtime($this->filePath);
        $this->seekManager = new SeekManager($seekManagerFile);
    }

    protected function isReady()
    {
        return !is_null($this->resource);
    }

    protected function open()
    {
        $this->close();

        $this->resource = fopen($this->filePath, 'r');

        $currentSeek = $this->seekManager->Seek();

        if ($currentSeek) {
            fseek($this->resource, $currentSeek);
            $this->filemtime = filemtime($this->filePath);
        }
    }

    protected function close()
    {
        if ($this->isReady()) {
            fclose($this->resource);
            $this->resource = null;
        }
    }

    public function rewind()
    {
        $this->seekManager->Seek(0);

        if ($this->isReady()) {
            rewind($this->resource);
        }
    }

    public function readline(): string
    {
        if (!$this->isReady()) {
            $this->open();
        }

        $line = fgets($this->resource);

        if ($line !== false) {
            $this->seekManager->Seek(ftell($this->resource));

            return $line;
        }

        $this->close();

        while ($this->filemtime >= filemtime($this->filePath)) {
            usleep($this->watchInterval);
            clearstatcache(true, $this->filePath);
        }

        return $this->readline();
    }
}

// $reader = new ContinuouslyFileReader(__DIR__ . DIRECTORY_SEPARATOR . "test.txt", 1000000);

// while($line = $reader->readline()) {
//     echo $line;
// }